﻿$(document).ready(function () {
    $('button').on('click', function () {
        if ($('#T_layerHeight').val() == "0" || ($('#T_layerHeight').val().includes("-")) || ($('#T_layerHeight').val().includes("e")))
            $('#T_layerHeight').addClass('inputError');
        if ($('#wid__B').val() == "0" || ($('#wid__B').val().includes("-")) || ($('#wid__B').val().includes("e")))
            $('#wid__B').addClass('inputError');
        if ($('#hei').val() == "0" || ($('#hei').val().includes("-")) || ($('#hei').val().includes("e")))
            $('#hei').addClass('inputError');
        if ($('input').hasClass('inputError')) {
            $('#error__message').show(200);
            return false;
        }
        return true;
    });

    $('input').on('click', function () {
        $('#error__message').hide(200);
        if ($('input').hasClass('inputError'))
            $('input').removeClass('inputError');
    });
    return false;
});