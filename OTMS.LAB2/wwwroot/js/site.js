﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
$(document).ready(function () {
    $('button').on('click', function () {
        if ($('#wid__T').val() == "0" || ($('#wid__T').val().includes("-")) || ($('#wid__T').val().includes("e")))
            $('#wid__T').addClass('inputError');
        if ($('#wid__B').val() == "0" || ($('#wid__B').val().includes("-")) || ($('#wid__B').val().includes("e")))
            $('#wid__B').addClass('inputError');
        if ($('#hei').val() == "0" || ($('#hei').val().includes("-")) || ($('#hei').val().includes("e")))
            $('#hei').addClass('inputError');
        if ($('input').hasClass('inputError')) {
            $('#error__message').show(200);
            return false;
        }
        return true;
    });

    $('input').on('click', function () {
        $('#error__message').hide(200);
        if ($('input').hasClass('inputError'))
            $('input').removeClass('inputError');
    });
    return false;
});