﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTMS.LAB2
{
    public class CalcParam
    {
        static public ViewParam calculating(Input it)
        {
            if (it.layerHeight <= 0)
            {
                throw new ArgumentException(nameof(it.layerHeight));
            }
            if (it.gasVelocity <= 0)
            {
                throw new ArgumentException(nameof(it.gasVelocity));
            }
            if (it.averageWarm <= 0)
            {
                throw new ArgumentException(nameof(it.averageWarm));
            }
            if (it.materialConsumption <= 0)
            {
                throw new ArgumentException(nameof(it.materialConsumption));
            }
            if (it.averageMaterialEating <= 0)
            {
                throw new ArgumentException(nameof(it.averageMaterialEating));
            }
            if (it.WarmReturn <= 0)
            {
                throw new ArgumentException(nameof(it.WarmReturn));
            }
            if (it.agregateDiametr <= 0)
            {
                throw new ArgumentException(nameof(it.agregateDiametr));
            }
            ViewParam vp = new ViewParam();
            vp.SquareS = Math.Round(Math.PI * it.agregateDiametr * it.agregateDiametr / 4, 3);
            vp.GasConsuption = Math.Round(it.gasVelocity * vp.SquareS, 3);
            vp.HeatRatio = Math.Round((it.materialConsumption * it.averageMaterialEating) / (vp.GasConsuption * it.averageWarm), 3);
            vp.FullHight = Math.Round(it.WarmReturn * it.layerHeight * vp.SquareS / (it.gasVelocity * vp.SquareS * it.averageWarm * 1000), 3);
            vp.HardExpression = Math.Round(1 - vp.HeatRatio * Math.Exp(-1 * (1 - vp.HeatRatio) * vp.FullHight / vp.HeatRatio), 3);
            List<double> l1 = new List<double>();
            List<double> l2 = new List<double>();
            List<double> l3 = new List<double>();
            List<double> l4 = new List<double>();
            List<double> l5 = new List<double>();
            List<double> l6 = new List<double>();
            List<double> l7 = new List<double>();
            List<double> l8 = new List<double>();
            for (double i = 0; i <= it.layerHeight; i += 0.5)
            {
                double tabline1 = Math.Round(it.WarmReturn * i / (it.gasVelocity * it.averageWarm * 1000), 3);
                l1.Add(tabline1);

                double tabline2 = Math.Round(1 - Math.Exp((vp.HeatRatio - 1) * tabline1 / vp.HeatRatio), 3);
                l2.Add(tabline2);

                l3.Add(Math.Round(1 - vp.HeatRatio * Math.Exp((vp.HeatRatio - 1) * tabline1 / vp.HeatRatio), 3));

                double tabline4 = Math.Round(tabline2 / (1 - vp.HeatRatio * Math.Exp((vp.HeatRatio - 1) * vp.FullHight / vp.HeatRatio)), 3);
                l4.Add(tabline4);

                double tabline5 = Math.Round((1 - vp.HeatRatio * Math.Exp((vp.HeatRatio - 1) * (tabline1 / vp.HeatRatio))) / (1 - vp.HeatRatio * Math.Exp((vp.HeatRatio - 1) * vp.FullHight / vp.HeatRatio)), 3);
                l5.Add(tabline5);

                double tabline6 = Math.Round(it.TemperatyreMaterial + (it.TemperatyreGas - it.TemperatyreMaterial) * tabline4, 3);
                l6.Add(tabline6);

                double tabline7 = Math.Round(it.TemperatyreMaterial + (it.TemperatyreGas - it.TemperatyreMaterial) * tabline5, 3);
                l7.Add(tabline7);

                double tabline8 = Math.Round(Math.Abs(tabline6 - tabline7), 3);
                l8.Add(tabline8);
            }

            vp.line1 = l1;
            vp.line2 = l2;
            vp.line3 = l3;
            vp.line4 = l4;
            vp.line5 = l5;
            vp.line6 = l6;
            vp.line7 = l7;
            vp.line8 = l8;
            vp.Resulttable[0] = vp.line1;
            vp.Resulttable[1] = vp.line2;
            vp.Resulttable[2] = vp.line3;
            vp.Resulttable[3] = vp.line4;
            vp.Resulttable[4] = vp.line5;
            vp.Resulttable[5] = vp.line6;
            vp.Resulttable[6] = vp.line7;
            vp.Resulttable[7] = vp.line8;





            return vp;
        }


    }
    }
