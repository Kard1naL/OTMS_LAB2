﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OTMS.LAB2.Models;

namespace OTMS.LAB2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public ActionResult Calc()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Calc(Input ip)   
        {

            try
            {
                ViewParam view = CalcParam.calculating(ip);
                ViewBag.Data = view;
                ViewBag.Input = ip;
                return View("Calc");
            }

            catch (ArgumentException)

            {
                return View("Index");
            }


        }
       
    }
}
