﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTMS.LAB2
{
    public class ViewParam
    {
        public double SquareS { get; set; }
        public double GasConsuption { get; set; }
        public double HeatRatio { get; set; }
        public double FullHight { get; set; }
        public double HardExpression { get; set; }

        public List<double> line1 { get; set; }
        public List<double> line2 { get; set; }
        public List<double> line3 { get; set; }
        public List<double> line4 { get; set; }
        public List<double> line5 { get; set; }
        public List<double> line6 { get; set; }
        public List<double> line7 { get; set; }
        public List<double> line8 { get; set; }

        public List<double>[] Resulttable = new List<double>[8];


        
    }

}
